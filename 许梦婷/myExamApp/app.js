'use strict'

let Koa=require('koa');
let bodyParser=require('koa-bodyparser')
let temp=require('./templating');
let controllers=require('./controllers');
let koa_static=require('koa-static');

let app =new Koa();
app.use(koa_static(__dirname));
app.use(bodyParser());
app.use(temp);
app.use(controllers());
app.use(temp);

app.use(async(ctx,next)=>{
    ctx.render('hello.html',{nickname:'好好学习<script>alert("天天向上")</script>'});

})


let port=3000;
app.listen(port);
console.log(`http://127.0.0.1:${port}`);


