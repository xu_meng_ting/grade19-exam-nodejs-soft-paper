'use strict'
let nunjucks=require('nunjucks');
//创建nunjucks的Environment的实例
function createEnv(path,opts){
path=path || 'views';
opts=opts || {};
console.log(opts);

let envOptions={
    autoescape :opts.autoescape===undefined?true:opts.autoescape,
    throwOnUndefined :opts.throwOnUndefined===undefined?false:opts.throwOnUndefined,
    trimBlocks :opts.trimBlocks===undefined?false:opts.trimBlocks,
    lstripBlocks:opts.lstripBlocks===undefined?false:opts.lstripBlocks,
    watch:opts.watch===undefined?true:opts.watch,
    noCache :opts.noCache===undefined?true:opts.noCache,
}
console.log(envOptions);
let env=nunjucks.configure(path,envOptions);
return env;
}
//暴露一个异步函数，作为koa实例的中间件
module.exports=async(ctx,next)=>{
    ctx.render=function(view,model){
      let env=createEnv('views',{autoescape:false});
      ctx.body=env.render(view,model);
    }
    await next();
}